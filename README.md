# projeteigenface

Projet de M1 Traitement du Signal et des Images

Implémentation de la détection de visages basée sur la méthode des eigenfaces.

ATTENTION : Les programmes sont écrits en Python 2 !

## Classe FaceModel

La classe FaceModel implémente la définition d'un modèle statistique qui définit une base d'apprentissage.

Le chargement des fichiers images qui consituent la base d'apprentissage se fait par la méthode load.
La classe FaceModel ne s'utilise pas seule, elle sert de classe abstraite pour ses deux classes filles : **EigenFaceModel** et **FisherFaceModel**.

## Classe **EigenFaceModel**
Cette classe permet de définir le modèle selon la méthode des eigenfaces. Elle hérite de FaceModel.

## Classe **FisherFaceModel**
Cette classe permet de définir le modèle selon un classifieur de Fisher. Elle hérite également de FaceModel.


## Utilisation
~~~~{.python}
fm = FisherFaceModel()

# path est le chemin vers le dossier des images
fm.load(path)

# calcul du modele
# l'argument 415 correspond au nombre d'images utilisées
[eigenvalues, eigenvectors, mu] = fm.compute(415)

#Projection d'un image 
# img est une image chargée précédemment
# nbProj est le nombre de valeurs propres pour la projection
proj = fm.project(img,nbProj)

# reconstruction a partir de la derniere projection realisée
imgproj = fm.reconstruct()
~~~~


## Programme exemple : détection de classe
Lancer le programme :

~~~~{.bash}
python rechercheClasse.py <chemin dossier images d'apprentissage> <nombre de valeurs propres> <modèle>
~~~~
<modèle> peut prendre les valeurs : 'eigenface', 'fisherface'

Lancer la commande sans argument permet d'obtenir l'aide !

Une fois la commande lancée, on saisit le nom de l'image dont on veut déterminer la classe. Les images à tester doivent se trouver dans le dossier imgtest.
Lorsque le fichier image est lu, on choisit ensuite le type de distance à utiliser.

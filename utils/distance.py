# coding: utf8
import numpy as np

def malahanobisDistance(u,v,eigenvalues):
	"""
	@param u array
	@param v array
	@brief
	Compute the Malahanobis distance between u and v
	"""
	u = np.asarray(u)
	v = np.asarray(v)
	eigenvalues = np.sqrt(np.abs(eigenvalues))
	d = 0
	for i in range(len(u)):
		tmp = u[i]-v[i];
		tmp = np.power(tmp,2)
		tmp /= eigenvalues[i]
		d += tmp
	return np.sqrt(d.item(0))

def normEudlidianDistance(u,v):
	"""
	@param u array
	@param v array
	@brief
	Compute the Malahanobis distance between u and v
	"""
	u = np.asarray(u)
	v = np.asarray(v)
	d = 0
	uvar = np.var(u)
	for i in range(len(u)):
		tmp = u[i]-v[i];
		tmp = np.power(tmp,2)
		d += tmp
	tmp /= uvar
	return d.item(0)

class DistanceComputer:
	def __init__(self,eigenvalues):
		self.eigenvalues = eigenvalues
	def compute(self,distName,u,v):
		try:
			distFunc = getattr(self,distName + 'Distance')
		except(AttributeError):
			return None
		if distFunc != None:
			return distFunc(u,v)
		
	def mahalanobisDistance(self,u,v):
		"""
		@param u array
		@param v array
		@brief
		Compute the Malahanobis distance between u and v
		"""
		u = np.asarray(u)
		v = np.asarray(v)
		eigenvalues = np.sqrt(np.abs(self.eigenvalues))
		d = 0
		for i in range(u.shape[1]):
			tmp = u[0,i]-v[0,i];
			tmp = np.power(tmp,2)
			tmp /= eigenvalues[i]
			d += tmp
		return np.sqrt(d.item(0))
	
	def normEuclidianDistance(self,u,v):
		"""
		@param u array
		@param v array
		@brief
		Compute the Malahanobis distance between u and v
		"""
		u = np.asarray(u)
		v = np.asarray(v)
		d = 0
		uvar = np.var(u)
		for i in range(u.shape[1]):
			tmp = u[0,i]-v[0,i];
			tmp = np.power(tmp,2)
			d += tmp
		tmp /= uvar
		return d.item(0)
	def euclidianDistance(self,u,v):
		return np.linalg.norm(u-v)
	def manhattanDistance(self,u,v):
		u = np.asarray(u)
		v = np.asarray(v)
		d = 0
		for i in range(u.shape[1]):
			tmp = np.abs(u[0,i]-v[0,i]);
			d += tmp
		return d.item(0)
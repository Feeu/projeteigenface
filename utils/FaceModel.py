# coding: utf8
import numpy as np
from .distance import *
from .imgfile import *
import matplotlib.pyplot as plt
import matplotlib.cm as cm

"""
	Classe abstraite de mocele statistique
"""
class FaceModel():
	def __init__(self):
		self.X = None # Images de la base
		self.Y = [] # Vecteur des classes d'images
		self.dim = None # dimension de chaque image
		self.distFunction = 'mahalnobis' # Fonction de distance utilisee
		self.vecs = None # vecteurs propres
		self.vals = None # valeurs propres
		self.mean = None # vecteur moyen
		self.y = None # derniere projection calculee
	def setDistFunction(self,distFunc):
		self.distFunction = distFunc;
	def setDirectory(self,dirPath):
		self.directory = dirPath
	def setImageDataSet(self,X,Y):
		self.X = X
		self.Y = Y
	def getFaceFilter(self):
		filtre = np.zeros(self.getDim())
		filtre[40:65,8:self.getDim()[1]-8] = 1
		filtre[65:80,35:55] = 1
		filtre = np.reshape(filtre,self.getRowDim())
		return filtre
	def applyFaceFilter(self):
		filtre = self.getFaceFilter()
		for i in range(len(self.X)):
			self.X[i] = np.multiply(self.X[i],filtre)
	def meanFace(self,X):
		return np.asarray(np.mean(X,0))[0]
	def stdFace(self,X):
		return np.asarray(np.std(X,0))[0]
	def load(self,path):
		[self.X,self.Y] = face_read(path)
		self.dim = self.X[0].shape
		self.X = asRowMatrix(self.X)
	def compute(self,k):
		raise NotImplementedError
	def project(self,x,n=None):
		"""
		Renvoie le vecteur de ponderations sur le facespace
		x => image en ligne
		v => matrice dont chaque ligne est un vecteur propre
		"""
		self.y = []
		self.y = np.dot(self.vecs[0:n,:],(x-self.mean).T)
		return self.y
	def printModel(self,k):
		plt.figure()
		nbRows = k / 10
		for i in range(k):
			plt.subplot(nbRows+1,10,i+1)
			plt.imshow(np.reshape(self.vecs[i],self.dim),cmap=cm.gray)
	def reconstruct(self,y=None):
		"""
		Reconstruit une image dans le face space avec un vecteur de ponderations
		"""
		if y == None:
			y = self.y
		img = np.zeros(self.vecs[0].shape)
		for i in range(len(y)):
			img = np.add(img,y[i]*self.vecs[i])
		return img + self.mean
	def getDim(self):
		return self.dim
	def getRowDim(self):
		return (1,self.dim[0] * self.dim[1])
	def classMeans(self,n):
		X = np.asarray(self.X)
		y_unique = np.unique(self.Y) # on cherche les différents indices de classes
		nbClass = len(y_unique) # on compte le nombre de classes differentes
		Xmean = np.zeros((nbClass,len(X[0])))
		print(Xmean.shape)
		for i in range(len(y_unique)):
			XInClass = np.where(self.Y==y_unique[i])
			XInClass = X[XInClass]
			nbFaceInClass = n
			if len(XInClass)<n:
				nbFaceInClass = len(XInClass)
			for j in range(nbFaceInClass):
				Xmean[i] += XInClass[j]
			Xmean[i] /= len(XInClass)
		return [Xmean,y_unique]	

	def setEyeModel(self):
		Xeye = []
		xa_eye = 30
		xb_eye = 65
		ya_eye = 9
		yb_eye = 80
		dim_eye = (xb_eye-xa_eye,yb_eye-ya_eye)
		dim_eye_row = (1,dim_eye[0]*dim_eye[1])
		for i in range(len(self.X)):
			Xtmp = np.reshape(self.X[i],self.getDim())
			Xtmp = Xtmp[xa_eye:xb_eye,ya_eye:yb_eye]
			Xeye.append(np.reshape(Xtmp,dim_eye_row))
		self.X = asRowMatrix(Xeye)
		self.dim = dim_eye
	def setMouthModel(self):
		Xmouth = []
		xa_mouth = 80
		xb_mouth = 100
		ya_mouth = 25
		yb_mouth = 65
		dim_mouth = (xb_mouth-xa_mouth,yb_mouth-ya_mouth)
		dim_mouth_row = (1,dim_mouth[0]*dim_mouth[1])
		for i in range(len(self.X)):
			Xtmp = np.reshape(self.X[i],self.getDim())
			Xtmp = Xtmp[xa_mouth:xb_mouth,ya_mouth:yb_mouth]
			Xmouth.append(np.reshape(Xtmp,dim_mouth_row))
		self.X = asRowMatrix(Xmouth)
		self.dim = dim_mouth
	
	def extractEye(self,x):
		xa_eye = 30
		xb_eye = 65
		ya_eye = 9
		yb_eye = 80
		return x[xa_eye:xb_eye,ya_eye:yb_eye]
	def extractMouth(self,x):
		xa_mouth = 80
		xb_mouth = 100
		ya_mouth = 25
		yb_mouth = 65
		return x[xa_mouth:xb_mouth,ya_mouth:yb_mouth]
	
"""
	Classe du modele eigenface
"""
class EigenFaceModel(FaceModel):
	def compute(self,k):
		
		n = len(self.X)
		d = self.X[0].size
		mean_face = self.meanFace(self.X)
		X = self.X - mean_face
		if n>d:
			C = np.dot(X.T,X)
			[self.vals,self.vecs] = np.linalg.eigh(C)
		else:
			C = np.dot(X,	X.T)
			[self.vals,self.vecs] = np.linalg.eigh(C)
			self.vecs = np.dot(X.T,self.vecs)
			for i in xrange(n):
				self.vecs[:,i] = self.vecs[:,i]/np.linalg.norm(self.vecs[:,i])
		idx = np.argsort(-self.vals)
		self.vals = self.vals[idx]
		self.vecs = self.vecs[:,idx]
		self.vals = self.vals[0:k].copy()
		self.vecs = self.vecs[:,0:k].copy()
		self.vecs = np.asarray(self.vecs)
		self.vecs = self.vecs.T
		self.mean = mean_face
		return [self.vals, self.vecs, mean_face]

"""
	Classe de modele Fisherface
"""
class FisherFaceModel(FaceModel):
	def lda(self,X, y, num_components=0):
		y = np.asarray(y)
		[d,n] = X.shape
		c = np.unique(y) # contient les id de chaque classe
		if (num_components <= 0) or (num_components>(len(c)-1)):
			num_components = (len(c)-1)
		# image moyenne de toutes les images
		meanTotal = X.mean(axis=1)
		
	
		# d == nombre d'images
		Sw = np.zeros((d, d), dtype=np.float32)
		Sb = np.zeros((d, d), dtype=np.float32)
		X = X.T
		# pour chaque classe
		for i in c:
			# on recupere toutes les images de la classe i
			Xi = X[np.where(y==i)[0],:]
			# calcul image moyenne de la classe
			meanClass = Xi.mean(axis=0)
			# calcul des matrices SW et SB
			# Sw => covariance a l'interieur des classe
			# Sb => covariance entre les classes
			Sw = Sw + np.dot((Xi-meanClass).T, (Xi-meanClass))
			Sb = Sb + n * np.dot((meanClass - meanTotal).T, (meanClass - meanTotal))
		# on cherche les vecteurs propres de Sb/Sw
		eigenvalues, eigenvectors = np.linalg.eig(np.linalg.inv(Sw)*Sb)
		# tri des indices par valeurs propres decroissantes
		idx = np.argsort(-eigenvalues.real)
		# on met les valeurs / vecteurs en ordre decroissant avec les indices crees
		eigenvalues, eigenvectors = eigenvalues[idx], eigenvectors[:,idx]
	
		# on recupere le nombre de composantes demandées
		eigenvalues = np.array(eigenvalues[0:num_components].real, dtype=np.float32, copy=True)
		eigenvectors = np.array(eigenvectors[0:,0:num_components].real, dtype=np.float32, copy=True)
		return [eigenvalues, eigenvectors]
	
	def compute(self,k):
		# X matrice dont chaque ligne est une image
		# vecteur colonne des classes
		y = np.asarray(self.Y)
		[n,d] = self.X.shape
		c = len(np.unique(y))
		fm = EigenFaceModel()
		fm.setImageDataSet(self.X,self.Y)
		[eigenvalues_pca, eigenvectors_pca, mu_pca] = fm.compute((n-c))
		Xtmp = fm.project(self.X)
		[eigenvalues_lda, eigenvectors_lda] = lda(np.asarray(Xtmp), y, k)
		eigenvectors = np.dot(eigenvectors_lda.T,eigenvectors_pca)
		self.vecs = eigenvectors
		self.vals = eigenvalues_lda
		self.mean = mu_pca
		return [eigenvalues_lda, eigenvectors, mu_pca]
	

"""
def project(x,v,mean=None):
	y = []
	for ev in v:
		if mean is None:
			y.append(np.dot(ev,x.T))
		else:
			y.append(np.dot(ev,(x-mean).T))
	return y
	"""
	
def classMeans(X,y):
	"""
	@param y class vector
	@param X eigen vectors
	"""
	X = np.asarray(X)
	y_unique = np.unique(y) # on cherche les différents indices de classes
	nbClass = len(y_unique) # on compte le nombre de classes differentes
	Xmean = np.zeros((nbClass,len(X[0])))
	for i in range(len(y_unique)):
		XInClass = np.where(y==y_unique[i])
		XInClass = X[XInClass]
		for j in range(len(XInClass)):
			Xmean[i] += XInClass[j]
		Xmean[i] /= len(XInClass)
	return [Xmean,y_unique]

def classEigenMeans(X,y,n):
	"""
	@param y class vector
	@param X eigen vectors
	"""
	X = np.asarray(X)
	y_unique = np.unique(y) # on cherche les différents indices de classes
	nbClass = len(y_unique) # on compte le nombre de classes differentes
	Xmean = np.zeros((nbClass,len(X[0])))
	
	for i in range(len(y_unique)):
		XInClass = np.where(y==y_unique[i])
		XInClass = X[XInClass]
		nbFaceInClass = n
		if len(XInClass)<n:
			nbFaceInClass = len(XInClass)
		for j in range(nbFaceInClass):
			Xmean[i] += XInClass[j]
		Xmean[i] /= len(XInClass)
	return [Xmean,y_unique]

def lda(X, y, num_components=0):
	y = np.asarray(y)
	[d,n] = X.shape
	c = np.unique(y) # contient les id de chaque classe
	if (num_components <= 0) or (num_components>(len(c)-1)):
		num_components = (len(c)-1)
	# image moyenne de toutes les images
	meanTotal = X.mean(axis=1)
	

	# d == nombre d'images
	Sw = np.zeros((d, d), dtype=np.float32)
	Sb = np.zeros((d, d), dtype=np.float32)
	X = X.T
	# pour chaque classe
	for i in c:
		# on recupere toutes les images de la classe i
		Xi = X[np.where(y==i)[0],:]
		# calcul image moyenne de la classe
		meanClass = Xi.mean(axis=0)
		# calcul des matrices SW et SB
		# Sw => covariance a l'interieur des classe
		# Sb => covariance entre les classes
		Sw = Sw + np.dot((Xi-meanClass).T, (Xi-meanClass))
		Sb = Sb + n * np.dot((meanClass - meanTotal).T, (meanClass - meanTotal))
	# on cherche les vecteurs propres de Sb/Sw
	eigenvalues, eigenvectors = np.linalg.eig(np.linalg.inv(Sw)*Sb)
	# tri des indices par valeurs propres decroissantes
	idx = np.argsort(-eigenvalues.real)
	# on met les valeurs / vecteurs en ordre decroissant avec les indices crees
	eigenvalues, eigenvectors = eigenvalues[idx], eigenvectors[:,idx]

	# on recupere le nombre de composantes demandées
	eigenvalues = np.array(eigenvalues[0:num_components].real, dtype=np.float32, copy=True)
	eigenvectors = np.array(eigenvectors[0:,0:num_components].real, dtype=np.float32, copy=True)
	return [eigenvalues, eigenvectors]

def fisherfaces(X,y,num_components=0):
	# X matrice dont chaque ligne est une image
	# vecteur colonne des classes
	print(X.shape)
	y = np.asarray(y)
	print(y.shape)
	[n,d] = X.shape
	c = len(np.unique(y))
	print(X)
	[eigenvalues_pca, eigenvectors_pca, mu_pca] = compute(X, y, (n-c))
	print(type(eigenvectors_pca))
	print(type(project(X,eigenvectors_pca,len(eigenvectors_pca),mu_pca)))
	[eigenvalues_lda, eigenvectors_lda] = lda(np.asarray(project(X,eigenvectors_pca,len(eigenvectors_pca),mu_pca)), y, num_components)
	eigenvectors = np.dot(eigenvectors_lda.T,eigenvectors_pca)
	return [eigenvalues_lda, eigenvectors, mu_pca]

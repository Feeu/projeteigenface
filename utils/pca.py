# coding: utf8
import numpy as np

def meanFace(X):
	return np.asarray(np.mean(X,0))[0]
	

def compute(X,y,k):
	n = len(X)
	d = X[0].size
	mean_face = meanFace(X)
	X = X - mean_face
	
	if n>d:
		C = np.dot(X.T,X)
		[eigenvalues,eigenvectors] = np.linalg.eigh(C)
	else:
		C = np.dot(X,X.T)
		[eigenvalues,eigenvectors] = np.linalg.eigh(C)
		eigenvectors = np.dot(X.T,eigenvectors)
		for i in xrange(n):
			eigenvectors[:,i] = eigenvectors[:,i]/np.linalg.norm(eigenvectors[:,i])
	idx = np.argsort(-eigenvalues)
	eigenvalues = eigenvalues[idx]
	eigenvectors = eigenvectors[:,idx]
	eigenvalues = eigenvalues[0:k].copy()
	eigenvectors = eigenvectors[:,0:k].copy()
	v = np.asarray(eigenvectors)
	v = v.T
	return [eigenvalues, v, mean_face]
	

"""
def compute(X, y, num_components=0):
	[n,d] = X.shape
	if (num_components <= 0) or (num_components>n):
		num_components = n
	mu = X.mean(axis=0)
	X = X - mu
	if n>d:
		C = np.dot(X.T,X)
		[eigenvalues,eigenvectors] = np.linalg.eigh(C)
	else:
		C = np.dot(X,X.T)
		[eigenvalues,eigenvectors] = np.linalg.eigh(C)
		eigenvectors = np.dot(X.T,eigenvectors)
		for i in xrange(n):
			eigenvectors[:,i] = eigenvectors[:,i]/np.linalg.norm(eigenvectors[:,i])
	# or simply perform an economy size decomposition
	# eigenvectors, eigenvalues, variance = np.linalg.svd(X.T, full_matrices=False)
	# sort eigenvectors descending by their eigenvalue
	idx = np.argsort(-eigenvalues)
	eigenvalues = eigenvalues[idx]
	eigenvectors = eigenvectors[:,idx]
	# select only num_components
	eigenvalues = eigenvalues[0:num_components].copy()
	eigenvectors = eigenvectors[:,0:num_components].copy()
	eigenvectors = eigenvectors.T
	return [eigenvalues, eigenvectors, mu]
"""
def project(x,v,n,mean=None):
	"""
	Renvoie le vecteur de ponderations sur le facespace
	x => image en ligne
	v => matrice dont chaque ligne est un vecteur propre
	"""
	y = []
	y = np.dot(v[0:n,:],(x-mean).T)
#	for i in range(n):
##	for ev in v:
##		print(v[i])
#		if mean is None:
#			y.append(np.dot(v[i,:],x.T))
#		else:
#			y.append(np.dot(v[i,:],(x-mean).T))
	return y
"""
def project(x,v,mean=None):
	y = []
	for ev in v:
		if mean is None:
			y.append(np.dot(ev,x.T))
		else:
			y.append(np.dot(ev,(x-mean).T))
	return y
	"""
def reconstruct(y,v,mean):
	"""
	Reconstruit une image dans le face space avec un vecteur de ponderations
	"""
	img = np.zeros(v[0].shape)
	for i in range(len(y)):
		img = np.add(img,y[i]*v[i])
	
	return img
	
def classMeans(X,y):
	"""
	@param y class vector
	@param X eigen vectors
	"""
	X = np.asarray(X)
	y_unique = np.unique(y) # on cherche les différents indices de classes
	nbClass = len(y_unique) # on compte le nombre de classes differentes
	Xmean = np.zeros((nbClass,len(X[0])))
	for i in range(len(y_unique)):
		XInClass = np.where(y==y_unique[i])
		XInClass = X[XInClass]
		for j in range(len(XInClass)):
			Xmean[i] += XInClass[j]
		Xmean[i] /= len(XInClass)
	return [Xmean,y_unique]

def classEigenMeans(X,y,n):
	"""
	@param y class vector
	@param X eigen vectors
	"""
	X = np.asarray(X)
	y_unique = np.unique(y) # on cherche les différents indices de classes
	nbClass = len(y_unique) # on compte le nombre de classes differentes
	Xmean = np.zeros((nbClass,len(X[0])))
	
	for i in range(len(y_unique)):
		XInClass = np.where(y==y_unique[i])
		XInClass = X[XInClass]
		nbFaceInClass = n
		if len(XInClass)<n:
			nbFaceInClass = len(XInClass)
		for j in range(nbFaceInClass):
			Xmean[i] += XInClass[j]
		Xmean[i] /= len(XInClass)
	return [Xmean,y_unique]

def lda(X, y, num_components=0):
	y = np.asarray(y)
	[d,n] = X.shape
	c = np.unique(y) # contient les id de chaque classe
	if (num_components <= 0) or (num_components>(len(c)-1)):
		num_components = (len(c)-1)
	# image moyenne de toutes les images
	meanTotal = X.mean(axis=1)
	

	# d == nombre d'images
	Sw = np.zeros((d, d), dtype=np.float32)
	Sb = np.zeros((d, d), dtype=np.float32)
	X = X.T
	# pour chaque classe
	for i in c:
		# on recupere toutes les images de la classe i
		Xi = X[np.where(y==i)[0],:]
		# calcul image moyenne de la classe
		meanClass = Xi.mean(axis=0)
		# calcul des matrices SW et SB
		# Sw => covariance a l'interieur des classe
		# Sb => covariance entre les classes
		Sw = Sw + np.dot((Xi-meanClass).T, (Xi-meanClass))
		Sb = Sb + n * np.dot((meanClass - meanTotal).T, (meanClass - meanTotal))
	# on cherche les vecteurs propres de Sb/Sw
	eigenvalues, eigenvectors = np.linalg.eig(np.linalg.inv(Sw)*Sb)
	# tri des indices par valeurs propres decroissantes
	idx = np.argsort(-eigenvalues.real)
	# on met les valeurs / vecteurs en ordre decroissant avec les indices crees
	eigenvalues, eigenvectors = eigenvalues[idx], eigenvectors[:,idx]

	# on recupere le nombre de composantes demandées
	eigenvalues = np.array(eigenvalues[0:num_components].real, dtype=np.float32, copy=True)
	eigenvectors = np.array(eigenvectors[0:,0:num_components].real, dtype=np.float32, copy=True)
	return [eigenvalues, eigenvectors]

def fisherfaces(X,y,num_components=0):
	# X matrice dont chaque ligne est une image
	# vecteur colonne des classes
	print(X.shape)
	y = np.asarray(y)
	print(y.shape)
	[n,d] = X.shape
	c = len(np.unique(y))
	print(X)
	[eigenvalues_pca, eigenvectors_pca, mu_pca] = compute(X, y, (n-c))
	print(type(eigenvectors_pca))
	print(type(project(X,eigenvectors_pca,len(eigenvectors_pca),mu_pca)))
	[eigenvalues_lda, eigenvectors_lda] = lda(np.asarray(project(X,eigenvectors_pca,len(eigenvectors_pca),mu_pca)), y, num_components)
	eigenvectors = np.dot(eigenvectors_lda.T,eigenvectors_pca)
	return [eigenvalues_lda, eigenvectors, mu_pca]
# coding: utf8
import os
import sys

import utils.imgfile as imgfile

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from utils.distance import *

import utils.pca as pca

import numpy as np
from PIL import Image as image
from utils.FaceModel import EigenFaceModel
from utils.FaceModel import FisherFaceModel

plt.close('all')
if len(sys.argv) < 4:
    sys.stdout.write("Utilisation de la commande : \n")
    sys.stdout.write("---------------------------- \n\n")
    sys.stdout.write("python rechercheClasse.py <chemin dossier images> <nombre de valeurs propres> <modèle>\n")
    sys.stdout.write("Valeurs possibles pour le choix du modèle : 'eigenface', 'fisherface'\n\n")
    sys.stdout.write("=======================================================================================\n\n")
    sys.stdout.write("Un quatrième argument optionnel permet de définir un filtre à appliquer aux images :\n")
    sys.stdout.write("Utilisation de la commande : \npython rechercheClasse.py <chemin dossier images> <nombre de valeurs propres> <modèle> <filtre>\n")
    sys.stdout.write("Valeurs possibles du filtre : 'face'\n")
    exit()
###############################################################################
# Lecture des arguments, dossier de la base d'apprentissage
# Et nombre d'images à utiliser
###############################################################################
path = sys.argv[1]
nbEigenValues = int(sys.argv[2])
model = sys.argv[3]

nbProj = 400

if model == 'eigenface':
    fm = EigenFaceModel()
elif model == 'fisherface':
    fm = FisherFaceModel()
else:
    sys.stdout.write("ERREUR NOM DU MODELE !\nValeurs possibles pour le choix du modèle : 'eigenface', 'fisherface'\n\n")
    exit()

###############################################################################
# Lecture des images de la base
###############################################################################
[X,Y] = imgfile.face_read(path)
fm.load(path)

if len(sys.argv)==5:
    filt = sys.argv[4]
    if filt == 'face':
        fm.applyFaceFilter()
    else:
        sys.stdout.write("ERREUR NOM DU FILTRE !\nValeurs possibles pour le choix du filtre : 'face'\n\n")
        exit()

###############################################################################
# ANALYSE EN COMPOSANTES PRINCIPALES
# Puis conversions diverses ...
###############################################################################
[eigenvalues, eigenvectors, mu] = fm.compute(nbEigenValues)
dist = DistanceComputer(eigenvalues)
X = imgfile.asRowMatrix(X)
v = np.asarray(eigenvectors)

[Xmean,XmeanClass] = fm.classMeans(20)
plt.figure();
nbMeans = len(Xmean)
nbRows = nbMeans / 10 + (nbMeans % 10)
i = 0
for x in Xmean:
    plt.subplot(nbRows,10,i+1)
    plt.imshow(np.reshape(x,(fm.getDim())),cmap=cm.gray)
    i += 1
plt.show(block=False)
plt.suptitle('Visage moyen de chaque classe')

N = 15

distTypeList = ['manhattan','euclidian','normEuclidian','mahalanobis']
while 1:
    ##########################################################################
    # Lecture du fichier
    # On refuse si l'ouverture renvoie une erreur
    # Et on passe à l'iteration suivante ...
    ##########################################################################
    inputString = raw_input('Entrer le nom du fichier et le nombre de projections : ')

    inputString = inputString.split()
    if len(inputString)!=1:
        continue
    fichier = inputString[0]
    path = os.path.join('imgtest',fichier)
    try:
        face_img = image.open(path)
    except Exception as e:
        print 'Erreur ouverture fichier ...'
        continue
    while 1:
        inputString = raw_input('Choix de la distance\n1 - Manhattan\n2 - Euclidienne\n3 - Euclidienne normalisée\n4 - Mahalanobis\n')
        try:
            distTypeNumber = int(inputString)
            distType = distTypeList[distTypeNumber-1]
            break
        except Exception as e:
            print 'Erreur de saisie'
            continue

    
    ##########################################################################
    # Conversion, projection et reconstruction de l'image
    ##########################################################################
    img = face_img.convert("L") # conversion en niveaux de gris
    img = np.asarray(face_img, dtype=np.uint8) # image --> array
    img = np.reshape(img,(1,img.shape[0]*img.shape[1]))

    ##########################################################################
    # Calcul des projections des moyennes avec nbProj eigenfaces
    ##########################################################################
    XmeanProj = fm.project(Xmean,nbEigenValues)

    #
    proj = fm.project(img)
    imgproj = fm.reconstruct()
    plt.figure(2)
    plt.imshow(np.reshape(imgproj,(fm.getDim())))
    plt.show(block=False)
    plt.title('Image reconstruite')

    proj = np.reshape(proj,(1,nbEigenValues));
    dmin = sys.maxint
    for i in range(XmeanProj.shape[1]):
        d = dist.compute(distType,proj,np.reshape(XmeanProj[:,i],(1,nbEigenValues)))

        if d < dmin:
            dmin = d
            cmin = XmeanClass[i]
    print('Classe détectée : ' + str(cmin) + ' avec une distance de ' + str(dmin))